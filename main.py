#!/usr/bin/Python

# User level Scheduler for Parallel I/Os to NVM

import string
import sys
import re
import os
import datetime
import subprocess
import threading

sys.path.append(os.getcwd())
import scheduler_algorithms

global logfile = open("log_%s_%s.log"%(now.day, now.minute),'wb')

def helper(arg1, *args):
    """ helper print
    """
    print "The user-level scheduler for parallel IOs to NVM(s):"
    print "Usage: %s <mode> <runfile> <parallel/serial>"%args[1]
    print "<mode>, char: 'd' = default OS scheduler, 'b' = bind threads to cores, 'a' = animal scheduler, 'z' = distributed intensity scheduler, 'zo' = distributed intensity scheduler with online-based scheduling, 'p' = pacifier, 'c' = clustering threads for power savings, 'dino' = dino, 'q' = queue-based scheduling, 's' = swap threads to even out memory controller DRAM misses"
    print "<runfile>, string: list of benchmarks to run, when each of it should start to run (in seconds since the start of this program) along with their signatures and invocation strings"
    print "<parallel/serial>, char: 'p' = run all benchmarks in parallel, 's' = run all benchmarks in order"
    print "Required tools: perf, numactl"

def main():
    """ main function: pass the arguments, detect the architecture, schedule the benchmarks
    """
    mode = 'd'
    runfile = ""
    parallel = 'p'
    if not len(sys.argv) == 4:
        helper(sys.argv)
        sys.exit(0)
    [mode, runfile, parallel] = sys.argv[2:]
    commands = []
    fp = open(runfile)
    for line in fp:
        commands.append(line.split('\n')[0])

    # detecting architecture
    core_group = []
    (numa_information, stderr) = subprocess.Popen(["numactl","--hardware"], stdout=PIPE).communicate()
    for line in numa_information:
        if "cpus:" in line:
            cores = re.split(' |\n', line.split(':')[1])
            core_group.append(cores)

    processes = []
    # if parallel, submit all commands
    if parallel = 'p':
        for command in commands:
            processes.append(subprocess.Popen(command.split(), stdin = stdin, stdout = stdout)) 
        for process in processes:
            process.wait()
    else:
        for command in commands:
            process = subprocess.Popen(command.split(), stdin = stdin, stdout = stdout)
            process.wait()